#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <pthread.h>

#define A 630
#define SEED 2343
#define MAX_I 1
#define MAX_PROCS 128

/* Use macro instead of functions to save stack space */
#define map_op1(x)	pow(((x) / M_E), 1.0/3.0)
#define map_op2(x)	fabs(1/tan(x))

#ifdef _OPENMP
#include <omp.h>
#else
static volatile int threads_num = 1;

int omp_get_num_procs()
{
	return threads_num;
}

int omp_get_thread_num()
{
	return 0;
}

void omp_set_num_threads(int thrds)
{
	threads_num = thrds;
}

double omp_get_wtime()
{
	struct timeval T;
	double time_ms;

	gettimeofday(&T, NULL);
	time_ms = ((double) T.tv_sec) + ((double)T.tv_usec) / 1000000;
	return (double)time_ms;
} 
#endif 
/* Required global variables */
static unsigned char flag = 0;
static unsigned int iterator = 0;

struct loop_info {
	unsigned int from;
	unsigned int to;
	void* data;
	void (*work_do)(unsigned int i, void *data, unsigned int from);
};

struct loop_args {
	unsigned int n;
	double *array_m1;
	double *array_m2;
};

/* Do a part of loop from a to b */
void *thread_do(void *ptr)
{
	struct loop_info *info = (struct loop_info *) ptr;
	unsigned int i;

	for (i = info->from; i <= info->to; i++) {
		info->work_do(i, info->data, info->from);
	}

	free(info);
	return NULL;
}

void parallelize_loop(struct loop_args *aargs, void (*work_do)(unsigned int i, void *data, unsigned int from))
{
	unsigned int i;
	pthread_t gen_thread[MAX_PROCS];
	struct loop_info *info;
	int a, b, ret;
	int n = aargs->n;
	int num_procs = omp_get_num_procs();
	int curr_chunk = n % num_procs ? n / num_procs + 1 : n / num_procs;

	for (i = 0; i < num_procs; i++)
	{
		info = malloc(sizeof(struct loop_info));

		info->data = aargs;
		info->work_do = work_do;

		a = i * curr_chunk;
		info->from = a < n - 1 ? a : n - 1;
		b = i * curr_chunk  + curr_chunk - 1;
		info->to = b < n - 1 ? b : n - 1;

		ret = pthread_create(&gen_thread[i], NULL, thread_do, info);
		if(ret) {
			perror("Error when creating thread:");
			exit(EXIT_FAILURE);
		}
	}

	/* Wait for all threads to complete */
	for (i = 0; i < num_procs; i++)
	{
		ret = pthread_join(gen_thread[i], NULL);
		if(ret) {
			perror("Error when creating thread:");
			exit(EXIT_FAILURE);
		}
	}
}


inline void generate_do(unsigned int i, void* data, unsigned int from)
{
	struct loop_args *args = (struct loop_args *) data;
	unsigned int seed_local;
	unsigned int min, max;

	min = 1; max = A; seed_local = i * i * i;
	args->array_m1[i] = ((double)(rand_r(&seed_local)%(100*(max - min)))/100) + min;
	if (i < args->n / 2) {
		min = A; max = 10 * A; seed_local = i * i * i;
		args->array_m2[i] = ((double)(rand_r(&seed_local)%(100*(max - min)))/100) + min;
	}
}

void generate_array(struct loop_args *args)
{
	parallelize_loop(args, &generate_do);
}

inline void map_main_do(unsigned int i, void* data, unsigned int from)
{
	struct loop_args *args = (struct loop_args *) data;

	args->array_m1[i] = map_op1(args->array_m1[i]);
}

void map_main(struct loop_args *args)
{
	int i;

	parallelize_loop(args, &map_main_do);

	for (i = args->n/2 - 1; i > 0; i--) {
		args->array_m2[i] = args->array_m2[i] + args->array_m2[i - 1];
		args->array_m2[i] = map_op2(args->array_m2[i]);
	}
	args->array_m2[0] = map_op2(args->array_m2[0]);
}

inline void merge_main_do(unsigned int i, void* data, unsigned int from)
{
	struct loop_args *args = (struct loop_args *) data;	
	
	args->array_m2[i] = args->array_m1[i]/args->array_m2[i];
}

void merge_main(struct loop_args *args)
{
	parallelize_loop(args, &merge_main_do);
}

void insertion_sort(double *array, unsigned int a, unsigned int b)
{
	double new_element;
	unsigned int i, location;

	for (i = a; i <= b; i++) {
		new_element = array[i];
		location = i;
		while (location > a && (array[location - 1] > new_element))
		{
			array[location] = array[location - 1];
			location--;
		}
		array[location] = new_element;
	}
}

/* New array should be initialized */
void merge(double *array_old, double *array_new, unsigned int n, int num, int chunk) {
	unsigned int i, m;
	unsigned int *arr_i = calloc(num, (sizeof(unsigned int)));
	unsigned int min;

	for (i = 0; i < n; i++) {
		min = 0;
		for (m = 0; m < num; m++) {
			if (arr_i[m] < chunk) {
				array_new[i] = array_old[m*chunk + arr_i[m]];
				min = m;
				break;
			}
		}
		for (m = 0; m < num; m++) {
			if((m*chunk + arr_i[m] < n) && (arr_i[m] < chunk) && array_old[m*chunk + arr_i[m]] < array_new[i]) {
				array_new[i] = array_old[m*chunk + arr_i[m]];
				min = m;
			}
		}
		arr_i[min]++;
	}
}


inline void sort_do(unsigned int i, void* data, unsigned int from)
{

	struct loop_args *args = (struct loop_args *) data;	
	double new_element;
	unsigned int location;
	
	//fprintf(stderr, "from %u at stage %u\n", from, i);

	new_element = args->array_m2[i];
	location = i;
	while (location > from && (args->array_m2[location - 1] > new_element))
	{
		args->array_m2[location] = args->array_m2[location - 1];
		location--;
	}
	args->array_m2[location] = new_element;
}

void sort(struct loop_args *args)
{
	parallelize_loop(args, &sort_do);
	int num_procs = omp_get_num_procs();
	int curr_chunk = args->n % num_procs ? args->n / num_procs + 1 : args->n / num_procs;
	double *array_new = malloc(sizeof(double) * args->n);

	merge(args->array_m2, array_new, args->n, num_procs, curr_chunk);
	free(args->array_m2);
	args->array_m2 = array_new;
}

double reduce(double *arr, unsigned int n) {
	double res = 0, min = 0;
	int i = 0;

	while (arr[i] == 0)
		i++;
	min = arr[i];

	for (i = 0; i < n; i++) {
		if ((int)(arr[i] / min) % 2 == 0)
			res += sin(arr[i]);
	}

	return res;
}

void *print_load(void *ptr)
{
	double load;
	char cpu[1024];
	unsigned int c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, j;
	unsigned long long old[5];
	unsigned long long old_idle[5];
	unsigned long long new;
	unsigned long long new_idle;
	FILE *cpu_file = (FILE*) ptr;
	FILE *stat_file;

	stat_file = fopen("/proc/stat", "r");
	for (j = 0; j < 5; j++) {
		fscanf(stat_file, "%s %u %u %u %u %u %u %u %u %u %u\n", cpu, &c1, &c2, &c3, &c4, &c5, &c6, &c7, &c8, &c9, &c10);
		old[j] = c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8 + c9 + c10;
		old_idle[j] = c4;
	}
	fclose(stat_file);
	usleep(100);

	while(flag) {
		stat_file = fopen("/proc/stat", "r");
		for (j = 0; j < 5; j++) {
			fscanf(stat_file, "%s %u %u %u %u %u %u %u %u %u %u\n", cpu, &c1, &c2, &c3, &c4, &c5, &c6, &c7, &c8, &c9, &c10);
			new = c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8 + c9 + c10 - old[j];
			new_idle = c4 - old_idle[j];
			old_idle[j] = c4;
			old[j] = new + old[j];
			load = ((double) new - (double) new_idle) / (double) new;
			fprintf(cpu_file, "%f,", load);
		}
		fprintf(cpu_file, "\n");

		fclose(stat_file);
		usleep(100000);
	}

	fclose(cpu_file);
	return NULL;
}

void *print_done(void *ptr)
{
	FILE *log = fopen("done.log", "w");
	fprintf(log, "Task is completed for:\n0  %%");
	while(flag) {
		sleep(1);
		fprintf(log, "%c[2K\r%.0f %%", 27, 100.0*((double)iterator/(double)MAX_I));
		fflush(log);
	}
	fprintf(log, "\n");
	fclose(log);
	return NULL;
}

int main(int argc, char **argv)
{
	double dtime_ms, dminimal_time_ms = -1.0, gen_time = -1.0, map_time = -1.0, mer_time = -1.0, sor_time = -1.0;
	double *array_m1, *array_m2, x, start_time, end_time;
	int n, thrds, ret;
	pthread_t thread_done;
	struct loop_args *args = malloc(sizeof(struct loop_args));

	if (argc > 1)
		 args->n = n = atoi(argv[1]); /* инициализировать число N первым параметром командной строки */
	else args->n = n = 20;


	if (argc > 2)
		thrds = atoi(argv[2]); /* инициализировать число N первым параметром командной строки */
	else thrds = 4;
	omp_set_num_threads(thrds);

	flag = 1;
	ret = pthread_create(&thread_done, NULL, print_done, NULL);
	if(ret) {
		perror("Error when creating thread:");
		exit(EXIT_FAILURE);
	}


	for (iterator = 0; iterator < MAX_I; ++iterator) {

		args->array_m1 = array_m1 = malloc(args->n * sizeof(double));
		args->array_m2 = array_m2 = malloc((args->n / 2) * sizeof(double));

		/* Stage 1 */
		start_time = omp_get_wtime();
		generate_array(args);
		end_time = omp_get_wtime();
		dtime_ms = (end_time - start_time) * 1000;
		if ((gen_time == -1.0) || (dtime_ms < gen_time)) gen_time = dtime_ms;
		/* Stage 2 */
		start_time = omp_get_wtime();
		map_main(args);
		end_time = omp_get_wtime();
		dtime_ms = (end_time - start_time) * 1000;
		if ((map_time == -1.0) || (dtime_ms < map_time)) map_time = dtime_ms;

		args->n = n/2; /* This is important! */

		/* Stage 3 */
		start_time = omp_get_wtime();
		merge_main(args);
		end_time = omp_get_wtime();
		dtime_ms = (end_time - start_time) * 1000;
		if ((mer_time == -1.0) || (dtime_ms < mer_time)) mer_time = dtime_ms;
		/* Stage 4 */
		start_time = omp_get_wtime();
		sort(args);
		end_time = omp_get_wtime();
		dtime_ms = (end_time - start_time) * 1000;
		if ((sor_time == -1.0) || (dtime_ms < sor_time)) sor_time = dtime_ms;
		/* Stage 5 */
		x = reduce(args->array_m2, n/2);

		dminimal_time_ms = gen_time + map_time + mer_time + sor_time;
		free(args->array_m1);
		free(args->array_m2);
	}
	free(args);
	flag = 0;


	printf("%lf\n", gen_time); /* затраченное время */
	printf("%lf\n", map_time); /* затраченное время */
	printf("%lf\n", mer_time); /* затраченное время */
	printf("%lf\n", sor_time); /* затраченное время */
	printf("%lf\n", dminimal_time_ms); /* затраченное время */
	printf("%f\n", x);

	return 0;
}
