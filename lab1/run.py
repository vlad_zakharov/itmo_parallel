#!/usr/bin/python

"""run.py - run parallel labs

Usage: ./run.py (-o MIN -t MAX) [-h -c -O OPTIM ]

Options:
  -h, --help  		show this help message and exit
  -o, --n1 MIN  	n1
  -t, --n2 MAX  	n2
  -O, --optim OPTIM  	optimize with OPTIM [default: 0]
  -c, --clean           clean all
"""

import commands
import docopt
import getopt
import sys

args = docopt.docopt(__doc__);

optim = "OPTIM=-O{}".format(args['--optim']) if args['--optim'] else ""
n1 = int(args['--n1']) if args['--n1'] else None
n2 = int(args['--n2']) if args['--n2'] else None
file_list = []

# Step 1: Build
try:
    if(args['--clean']):
        ret = commands.getstatusoutput("make clean")
        if (ret[0] != 0):
            raise IOError(ret[0], ret[1])
        print ret[1]
        exit(0)
        
    ret = commands.getstatusoutput("make seq")
    if (ret[0] != 0):
        raise IOError(ret[0], ret[1])
    print ret[1]
    for line in ret[1].split('\n'):
        file_list.append(line.split(' ')[-1])

#     for schedule in ["static", "dynamic", "guided"]:
#         for chunk in [1, 4, 32, 512, 1048576, 2147483647]:
#             ret = commands.getstatusoutput("make seq {} SCHEDULE={} CHUNK={}".format(optim, schedule, chunk))
#             if (ret[0] != 0):
#                 raise IOError(ret[0], ret[1])
#             print ret[1]
#             for line in ret[1].split('\n'):
#                 file_list.append(line.split(' ')[-1])
except IOError as e:
    print "Error {}: {}".format(e.errno, e.strerror);
    exit(1)

print n1
print n2

# Step 2: run tests
delta = int((n2 - n1) / 20)
print delta
print file_list

for bin_file in file_list:
    with open('{}.csv'.format(bin_file), 'w+') as log_file:
        try:
            for n in range(n1, n2+1, delta):
                ret = commands.getstatusoutput('OMP_NESTED=true ./{} {}'.format(bin_file, n))
                if (ret[0] != 0):
                    raise IOError(ret[0], ret[1])

                log_file.write('{}'.format(n))
                for i in range(0,5):
                    log_file.write(',{}'.format(ret[1].split('\n')[i]))
                log_file.write('\n');
                log_file.flush()
        except IOError as e:
            print "Error {}: {}".format(e.errno, e.strerror);
            exit(1)

# with open('lab1-threads-num.csv', 'w+') as log_file:
#     try:
#         for threads in [1, 2, 3, 4, 8, 32]:
#             ret = commands.getstatusoutput('./{} {} {}'.format(file_list[0], 60000000, threads))
#             if (ret[0] != 0):
#                 raise IOError(ret[0], ret[1])
#             log_file.write('{},{}\n'.format(n, ret[1].split('\n')[1], ret[1].split('\n')[0]))
#             log_file.flush()
#     except IOError as e:
#         print "Error {}: {}".format(e.errno, e.strerror);
#         exit(1)

exit(0)
