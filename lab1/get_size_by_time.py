#!/usr/bin/python
import sys
import getopt
import commands


if __name__ == '__main__':
    options, _ = getopt.getopt(sys.argv[1:], 't:p:i:', ['time=', 'program=', 'increment='])

    for opt, arg in options:
        if opt in ('-t', '--time'):
                time = arg
        elif opt in ('-p', '--program'):
                program_name = arg
        elif opt in ('-i', '--increment'):
                increment = int(arg)

    i = 0
    while True:
        i += increment
        o = commands.getstatusoutput("./{name} {size}".format(name=program_name, size=i))[1].split('\n')[-1]
        with open('find_log.csv', 'a+') as log_file:
            log_file.write("{i},{o}\n".format(i=i,o=o))

        if int(o) >= int(time):
            print i
            break

    sys.exit(0)

